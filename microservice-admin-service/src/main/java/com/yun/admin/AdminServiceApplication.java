package com.yun.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author: yunjiang.chen
 * @Data:2023/3/19 23:49
 * @version: 0.0.1
 */

@SpringBootApplication(
        scanBasePackages = {
                "com.yun.admin",
                "com.yun.core"
        }
)
@MapperScan("com.yun.admin.infra.mapper")
@EnableEurekaClient
@EnableDiscoveryClient
public class AdminServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminServiceApplication.class,args);
    }
}
