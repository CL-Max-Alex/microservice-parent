package com.yun.admin.api.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yun.admin.api.dto.PermissionDTO;
import com.yun.admin.api.dto.RoleDTO;
import com.yun.admin.api.dto.UserDTO;
import com.yun.admin.app.service.IRoleService;
import com.yun.admin.app.service.IUserService;
import com.yun.admin.domain.entity.IPermission;
import com.yun.admin.domain.entity.IRole;
import com.yun.admin.domain.entity.IUser;
import com.yun.core.pagehelper.PageHelper;
import com.yun.core.pagehelper.domain.PageRequest;
import com.yun.core.utils.Results;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Api(
        tags = "AdminRoleController"
)
@RestController
@RequestMapping("/role")
public class AdminRoleController {


    @Autowired
    private IRoleService roleService;

    @ApiModelProperty("手动新增权限保存")
    @GetMapping("/save")
    public ResponseEntity<?> save(IRole role){
        roleService.save(role);
        return Results.success();
    }

    @ApiModelProperty("根据ID删除")
    @GetMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        roleService.delete(id);
        return Results.success();
    }

    @ApiModelProperty("更新")
    @GetMapping("/update")
    public ResponseEntity<?> update(IRole role){
        roleService.update(role);
        return Results.success();
    }

    @ApiModelProperty("基础查询")
    @GetMapping("/query")
    public ResponseEntity<Page<IRole>> query(RoleDTO roleDTO, PageRequest pageRequest){
        Results.success(PageHelper.doPage(pageRequest, () -> roleService.query(roleDTO)));
        return Results.success();
    }


}
