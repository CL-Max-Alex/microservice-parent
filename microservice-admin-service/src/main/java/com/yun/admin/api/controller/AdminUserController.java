package com.yun.admin.api.controller;

import com.yun.admin.api.dto.PermissionAssignmentDTO;
import com.yun.admin.api.dto.RoleAssignmentDTO;
import com.yun.admin.api.dto.UserDTO;
import com.yun.admin.app.service.IUserService;
import com.yun.admin.domain.entity.IUser;
import com.yun.core.domain.Page;
import com.yun.core.pagehelper.PageHelper;
import com.yun.core.pagehelper.domain.PageRequest;
import com.yun.core.utils.Results;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Api(
        tags = "AdminUserController"
)
@RestController
@RequestMapping("/user")
public class AdminUserController {

    @Autowired
    private IUserService userService;

    @ApiModelProperty("保存")
    @GetMapping("/save")
    public ResponseEntity<?> save(IUser user){
         userService.save(user);
        return Results.success();
    }

    @ApiModelProperty("根据ID删除")
    @GetMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        userService.delete(id);
        return Results.success();
    }

    @ApiModelProperty("更新")
    @GetMapping("/update")
    public ResponseEntity<?> update(IUser user){
        userService.update(user);
        return Results.success();
    }

    @ApiModelProperty("查询")
    @GetMapping("/query")
    public ResponseEntity<Page<IUser>> query(UserDTO userDTO, PageRequest pageRequest){
        return Results.success(PageHelper.doPage(pageRequest, () -> userService.query(userDTO)));
    }

    @ApiModelProperty("权限重置分配")
    @GetMapping("/permission-assignment")
    public ResponseEntity<?> permissionAssignment(PermissionAssignmentDTO permissionAssignmentDTO){
        userService.permissionAssignment(permissionAssignmentDTO);
        return Results.success();
    }


    @ApiModelProperty("角色重置分配")
    @GetMapping("/role-assignment")
    public void roleAssignment(RoleAssignmentDTO roleAssignmentDTO){
        userService.roleAssignment(roleAssignmentDTO);
    }



}
