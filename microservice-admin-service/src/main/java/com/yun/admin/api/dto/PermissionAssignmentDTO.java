package com.yun.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/29
 * 权限重置分配DTO
 */
@Data
public class PermissionAssignmentDTO {

    @ApiModelProperty("角色id")
    private Long roleId;

    @ApiModelProperty(value = "权限List")
    private List<Long> permIdList;

}
