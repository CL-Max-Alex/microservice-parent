package com.yun.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Data
public class PermissionDTO {

    @ApiModelProperty(value = "权限名称")
    private String permName;

    @ApiModelProperty(value = "权限编码")
    private String permCode;

    @ApiModelProperty(value = "用户是否启用。1启用，0未启用")
    private Integer isEnabled;

}
