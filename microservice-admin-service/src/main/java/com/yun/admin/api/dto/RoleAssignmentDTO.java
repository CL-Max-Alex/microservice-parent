package com.yun.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/29
 * 角色重置分配DTO
 */
@Data
public class RoleAssignmentDTO {

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty(value = "角色List")
    private List<Long> roleIdList;
}
