package com.yun.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Data
public class UserDTO {

    @ApiModelProperty(value = "用户名")
    private String loginName;

    @ApiModelProperty(value = "邮箱")
    private String email;


    @ApiModelProperty(value = "用户真实姓名")
    private String realName;
}
