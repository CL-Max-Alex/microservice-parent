package com.yun.admin.api.dto.expand;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Data
public class AttrDomain {
    /**
     * 备用字段
     */
    @ApiModelProperty("")
    private String attribute1;
    @ApiModelProperty("")
    private String attribute2;
    @ApiModelProperty("")
    private String attribute3;
    @ApiModelProperty("")
    private String attribute4;
    @ApiModelProperty("")
    private String attribute5;
    @ApiModelProperty("")
    private String attribute6;
    @ApiModelProperty("")
    private String attribute7;
    @ApiModelProperty("")
    private String attribute8;
    @ApiModelProperty("")
    private String attribute9;
    @ApiModelProperty("")
    private String attribute10;
    @ApiModelProperty("")
    private String attribute11;
    @ApiModelProperty("")
    private String attribute12;
    @ApiModelProperty("")
    private String attribute13;
    @ApiModelProperty("")
    private String attribute14;
    @ApiModelProperty("")
    private String attribute15;

}
