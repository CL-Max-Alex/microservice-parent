package com.yun.admin.app.service;

import com.yun.admin.api.dto.PermissionDTO;
import com.yun.admin.domain.entity.IPermission;
import com.yun.admin.domain.entity.IUser;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
public interface IPermissionService {
    /**
     * 手动新增权限保存
     * @param permission
     */
    void save(IPermission permission);

    /**
     * 根据ID删除
     * @param id
     */
    void delete(Long id);

    /**
     * 更新
     * @param permission
     */
    void update(IPermission permission);

    /**
     * 基础查询
     * @param permissionDTO
     * @return
     */
    List<IPermission> query(PermissionDTO permissionDTO);
}
