package com.yun.admin.app.service;

import com.yun.admin.api.dto.RoleDTO;
import com.yun.admin.domain.entity.IRole;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
public interface IRoleService {

    /**
     * 手动新增权限保存
     * @param role
     */
    void save(IRole role);

    /**
     * 根据ID删除
     * @param id
     */
    void delete(Long id);

    /**
     * 更新
     * @param role
     */
    void update(IRole role);

    /**
     * 基础查询
     * @param roleDTO
     * @return
     */
    List<IRole> query(RoleDTO roleDTO);
}
