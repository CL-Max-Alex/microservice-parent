package com.yun.admin.app.service;

import com.yun.admin.api.dto.PermissionAssignmentDTO;
import com.yun.admin.api.dto.RoleAssignmentDTO;
import com.yun.admin.api.dto.UserDTO;
import com.yun.admin.domain.entity.IUser;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
public interface IUserService {

    /**
     * 手动新增用户
     * @param user
     * @return
     */
    void save(IUser user);

    /**
     * 根据ID删除
     * @param id
     */
    void delete(Long id);


    void update(IUser user);

    /**
     * 基础查询
     * @param userDTO
     * @return
     */
    List<IUser> query(UserDTO userDTO);


    /**
     * 权限重置分配
     * @param permissionAssignmentDTO 权限重置分配DTO
     */
    void permissionAssignment(PermissionAssignmentDTO permissionAssignmentDTO);

    /**
     * 角色重置分配
     * @param roleAssignmentDTO 角色重置分配DTO
     */
    void roleAssignment(RoleAssignmentDTO roleAssignmentDTO);
}
