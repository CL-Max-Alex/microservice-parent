package com.yun.admin.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yun.admin.api.dto.PermissionDTO;
import com.yun.admin.app.service.IPermissionService;
import com.yun.admin.domain.entity.IPermission;
import com.yun.admin.domain.entity.IUser;
import com.yun.admin.infra.mapper.IPermissionMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Service
public class IPermissionServiceImpl implements IPermissionService {

    @Autowired
    private IPermissionMapper permissionMapper;
    @Override
    public void save(IPermission permission) {
        permissionMapper.insert(permission);
    }

    @Override
    public void delete(Long id) {
        permissionMapper.deleteById(id);
    }

    @Override
    public void update(IPermission permission) {
        permissionMapper.updateById(permission);
    }

    @Override
    public List<IPermission> query(PermissionDTO permissionDTO) {
        LambdaQueryWrapper<IPermission> queryWrapper = new LambdaQueryWrapper<>();
        // 拼接条件
        if(StringUtils.isNotEmpty(permissionDTO.getPermCode())){
            queryWrapper.like(IPermission::getPermCode,permissionDTO.getPermCode());
        }
        if(StringUtils.isNotEmpty(permissionDTO.getPermName())){
            queryWrapper.like(IPermission::getPermName,permissionDTO.getPermName());
        }
        return permissionMapper.selectList(queryWrapper);
    }
}
