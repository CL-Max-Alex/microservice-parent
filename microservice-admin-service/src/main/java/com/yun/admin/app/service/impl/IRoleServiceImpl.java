package com.yun.admin.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yun.admin.api.dto.RoleDTO;
import com.yun.admin.app.service.IRoleService;
import com.yun.admin.domain.entity.IPermission;
import com.yun.admin.domain.entity.IRole;
import com.yun.admin.infra.mapper.IRoleMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Service
public class IRoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleMapper roleMapper;

    @Override
    public void save(IRole role) {
        roleMapper.insert(role);
    }

    @Override
    public void delete(Long id) {
        roleMapper.deleteById(id);
    }

    @Override
    public void update(IRole role) {
        roleMapper.updateById(role);
    }

    @Override
    public List<IRole> query(RoleDTO roleDTO) {
        LambdaQueryWrapper<IRole> queryWrapper = new LambdaQueryWrapper<>();
        // 拼接条件
        if(StringUtils.isNotEmpty(roleDTO.getRoleCode())){
            queryWrapper.like(IRole::getRoleCode,roleDTO.getRoleCode());
        }
        if(StringUtils.isNotEmpty(roleDTO.getRoleName())){
            queryWrapper.like(IRole::getRoleName,roleDTO.getRoleCode());
        }
        return roleMapper.selectList(queryWrapper);
    }
}
