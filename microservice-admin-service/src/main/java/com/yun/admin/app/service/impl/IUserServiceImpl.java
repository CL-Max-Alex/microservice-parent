package com.yun.admin.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yun.admin.api.dto.PermissionAssignmentDTO;
import com.yun.admin.api.dto.RoleAssignmentDTO;
import com.yun.admin.api.dto.UserDTO;
import com.yun.admin.app.service.IUserService;
import com.yun.admin.domain.entity.IRolePermission;
import com.yun.admin.domain.entity.IUser;
import com.yun.admin.domain.entity.IUserRole;
import com.yun.admin.infra.mapper.IRolePermissionMapper;
import com.yun.admin.infra.mapper.IUserMapper;
import com.yun.admin.infra.mapper.IUserRoleMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/28
 */
@Service
public class IUserServiceImpl implements IUserService {

    @Autowired
    private IUserMapper userMapper;

    @Autowired
    private IRolePermissionMapper rolePermissionMapper;

    @Autowired
    private IUserRoleMapper userRoleMapper;


    @Override
    public void save(IUser user) {
        userMapper.insert(user);
    }


    @Override
    public void delete(Long id) {
        userMapper.deleteById(id);
    }

    @Override
    public void update(IUser user) {
        userMapper.updateById(user);
    }

    @Override
    public List<IUser> query(UserDTO userDTO) {
        LambdaQueryWrapper<IUser> queryWrapper = new LambdaQueryWrapper<>();
        // 拼接条件
        if(StringUtils.isNotEmpty(userDTO.getLoginName())){
            queryWrapper.like(IUser::getLoginName,userDTO.getLoginName());
        }
        if(StringUtils.isNotEmpty(userDTO.getRealName())){
            queryWrapper.like(IUser::getRealName,userDTO.getRealName());
        }

        return userMapper.selectList(queryWrapper);
    }

    /**
     * 批量插入
     * @param permissionAssignmentDTO 权限重置分配DTO
     */
    @Override
    public void permissionAssignment(PermissionAssignmentDTO permissionAssignmentDTO) {
        List<Long> permIdList = permissionAssignmentDTO.getPermIdList();
        Long roleId = permissionAssignmentDTO.getRoleId();
        if(CollectionUtils.isNotEmpty(permIdList)&& ObjectUtils.isNotEmpty(roleId)){
            permIdList.forEach(permId->{
                IRolePermission rolePermission = new IRolePermission();
                rolePermission.setRoleId(roleId);
                rolePermission.setPermId(permId);
                rolePermissionMapper.insert(rolePermission);
            });
        }
    }

    /**
     * 批量插入
     * @param roleAssignmentDTO 角色重置分配DTO
     */
    @Override
    public void roleAssignment(RoleAssignmentDTO roleAssignmentDTO) {
        List<Long> roleIdList = roleAssignmentDTO.getRoleIdList();
        Long userId = roleAssignmentDTO.getUserId();
        if(CollectionUtils.isNotEmpty(roleIdList)&& ObjectUtils.isNotEmpty(userId)){
            roleIdList.forEach(roleId->{
                IUserRole userRole = new IUserRole();
                userRole.setUserId(userId);
                userRole.setRoleId(roleId);
                userRoleMapper.insert(userRole);
            });
        }
    }

}
