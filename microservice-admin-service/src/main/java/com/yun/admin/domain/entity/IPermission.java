package com.yun.admin.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yun.admin.api.dto.expand.AttrDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.io.Serializable;


/**
 * 权限表(Permission)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-28 22:23:40
 */

@Data
@ApiModel("权限表")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName( "hyun_permission")
public class IPermission extends AttrDomain implements Serializable {
    private static final long serialVersionUID = -36120972992378632L;

    public static final String FIELD_ID = "id";
    public static final String FIELD_PERM_NAME = "permName";
    public static final String FIELD_PERM_CODE = "permCode";
    public static final String FIELD_IS_ENABLED = "isEnabled";

    @TableId
    private Long id;

    @ApiModelProperty(value = "权限名称")
    private String permName;

    @ApiModelProperty(value = "权限编码")
    private String permCode;

    @ApiModelProperty(value = "用户是否启用。1启用，0未启用")
    private Integer isEnabled;

}

