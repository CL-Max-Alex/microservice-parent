package com.yun.admin.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yun.admin.api.dto.expand.AttrDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 角色权限关联表(RolePermission)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-29 00:34:41
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("角色权限关联表")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("hyun_role_permission")
public class IRolePermission extends AttrDomain implements Serializable {
    private static final long serialVersionUID = -72874289770502782L;

    public static final String FIELD_R_ID = "roleId";
    public static final String FIELD_P_ID = "permId";

    @TableId
    private Long id;

    @ApiModelProperty("角色id")
    private Long roleId;

    @ApiModelProperty(value = "权限id")
    private Long permId;


}

