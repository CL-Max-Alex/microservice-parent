package com.yun.admin.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.admin.domain.entity.IPermission;

import java.util.List;

/**
 * 权限表(Permission)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-28 22:23:40
 */
public interface IPermissionMapper extends BaseMapper<IPermission> {

}

