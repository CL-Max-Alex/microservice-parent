package com.yun.admin.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.admin.domain.entity.IRole;

import java.util.List;

/**
 * 角色表(Role)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-28 22:24:36
 */
public interface IRoleMapper extends BaseMapper<IRole> {

}

