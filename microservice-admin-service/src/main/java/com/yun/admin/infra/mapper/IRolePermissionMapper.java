package com.yun.admin.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.admin.domain.entity.IRolePermission;

/**
 * 角色权限关联表(RolePermission)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-29 00:34:42
 */
public interface IRolePermissionMapper extends BaseMapper<IRolePermission> {
}

