package com.yun.admin.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.admin.domain.entity.IUser;

import java.util.List;

/**
 * (User)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-28 22:25:37
 */
public interface IUserMapper extends BaseMapper<IUser> {

}

