package com.yun.admin.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.admin.domain.entity.IUserRole;

/**
 * 用户角色关联表(UserRole)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-29 00:35:15
 */
public interface IUserRoleMapper extends BaseMapper<IUserRole> {

}

