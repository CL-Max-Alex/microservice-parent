package com.yun.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @LogRequired 自定义注解 ,用于标识该方法需要记录操作
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LogRequired {
    /**
     * 操作名称
     */
    String operateName() default "";

    /**
     * 请求参数DTO名称、用于映射参数Key
     */
    String requestBodyName() default "";
}
