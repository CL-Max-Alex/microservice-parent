package com.yun.core.convertor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.AbstractRefreshableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.lang.reflect.Field;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: 听钱塘信起
 * @Data:2023/3/22 23:27
 * @version: 0.0.1
 */
public class ApplicationContextHelper implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationContextHelper.class);
    private static DefaultListableBeanFactory springFactory;
    private static ApplicationContext context;

    public ApplicationContextHelper() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        setContext(applicationContext);
        if (applicationContext instanceof AbstractRefreshableApplicationContext) {
            AbstractRefreshableApplicationContext springContext = (AbstractRefreshableApplicationContext)applicationContext;
            setFactory((DefaultListableBeanFactory)springContext.getBeanFactory());
        } else if (applicationContext instanceof GenericApplicationContext) {
            GenericApplicationContext springContext = (GenericApplicationContext)applicationContext;
            setFactory(springContext.getDefaultListableBeanFactory());
        }

    }

    private static void setContext(ApplicationContext applicationContext) {
        context = applicationContext;
    }

    private static void setFactory(DefaultListableBeanFactory springFactory) {
        ApplicationContextHelper.springFactory = springFactory;
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public static void asyncStaticSetter(Class<?> type, Class<?> target, String targetField) {
        asyncStaticSetter(type, (String)null, target, targetField);
    }

    public static void asyncStaticSetter(Class<?> type, String beanName, Class<?> target, String targetField) {
        if (!setByField(type, beanName, target, targetField)) {
            AtomicInteger counter = new AtomicInteger(0);
            ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1, (r) -> {
                return new Thread(r, "sync-setter");
            });
            executorService.scheduleAtFixedRate(() -> {
                boolean success = setByField(type, beanName, target, targetField);
                if (success) {
                    executorService.shutdown();
                } else if (counter.addAndGet(1) > 60) {
                    LOGGER.error("Setter field [{}] in [{}] failure because timeout.", targetField, target.getName());
                    executorService.shutdown();
                }

            }, 0L, 2L, TimeUnit.SECONDS);
        }
    }

    private static boolean setByField(Class<?> type, String beanName, Class<?> target, String targetField) {
        if (getContext() == null) {
            return false;
        } else {
            try {
                Object obj = getBean(type, beanName);
                Field field = target.getDeclaredField(targetField);
                field.setAccessible(true);
                field.set(target, obj);
                LOGGER.info("Async set field [{}] in [{}] success by field.", targetField, target.getName());
                return true;
            } catch (NoSuchFieldException var6) {
                LOGGER.error("Not found field [{}] in [{}].", new Object[]{targetField, target.getName(), var6});
            } catch (NoSuchBeanDefinitionException var7) {
                LOGGER.error("Not found bean [{}] for [{}].", new Object[]{type.getName(), target.getName(), var7});
            } catch (Exception var8) {
                LOGGER.error("Async set field [{}] in [{}] failure by field. exception: {}", new Object[]{targetField, target.getName(), var8.getMessage()});
            }

            return false;
        }
    }

    private static Object getBean(Class<?> type, String beanName) {
        Object obj;
        if (beanName != null) {
            try {
                obj = getContext().getBean(beanName, type);
            } catch (NoSuchBeanDefinitionException var4) {
                obj = getContext().getBean(type);
                LOGGER.warn("getBean by beanName [{}] not found, then getBean by type.", beanName);
            }
        } else {
            obj = getContext().getBean(type);
        }

        return obj;
    }
}
