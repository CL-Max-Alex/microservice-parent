package com.yun.core.pagehelper;

import com.yun.core.domain.PageInfo;
import com.yun.core.pagehelper.domain.PageRequest;
import com.yun.core.pagehelper.domain.Sort;
import com.yun.core.pagehelper.page.PageMethod;
import com.yun.core.domain.Page;


import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/2
 */
public class PageHelper extends PageMethod {
    private static final ThreadLocal<Boolean> cacheCount = new ThreadLocal();

    public PageHelper() {
    }

    public static void openCountCache() {
        cacheCount.set(Boolean.TRUE);
    }

    public static void closeCountCache() {
        cacheCount.remove();
    }

    public static boolean isCountCache() {
        return Boolean.TRUE.equals(cacheCount.get());
    }

    public static <E> Page<E> doPage(int page, int size, Select select) {
        if (page >= 0 && size > 0) {
            startPage(page, size);
            return (Page)select.doSelect();
        } else {
            return selectAllAsOnePage(select);
        }
    }

    public static <E> Page<E> doPage(PageRequest pageRequest, Select select) {
        if (pageRequest.getPage() >= 0 && pageRequest.getSize() > 0) {
            startPage(pageRequest);
            return (Page)select.doSelect();
        } else {
            return selectAllAsOnePage(select);
        }
    }

    public static <E> Page<E> doPageAndSort(PageRequest pageRequest, Select select) {
        if (pageRequest.getPage() >= 0 && pageRequest.getSize() > 0) {
            startPageAndSort(pageRequest);
            return (Page)select.doSelect();
        } else {
            startSort(pageRequest.getSort());
            return selectAllAsOnePage(select);
        }
    }

    public static <E> List<E> doSort(Sort sort, Select select) {
        startSort(sort);
        return select.doSelect();
    }

    private static <E> Page<E> selectAllAsOnePage(Select select) {
        List list = select.doSelect();
        int total = list.size();
        return new Page(list, new PageInfo(0, total == 0 ? 1 : total), (long)total);
    }
}
