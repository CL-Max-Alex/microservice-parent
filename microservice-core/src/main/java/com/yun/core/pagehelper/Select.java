package com.yun.core.pagehelper;

import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/2
 */
@FunctionalInterface
public interface Select<E> {
    List<E> doSelect();
}
