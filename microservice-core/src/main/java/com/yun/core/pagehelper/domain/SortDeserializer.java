package com.yun.core.pagehelper.domain;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.yun.core.pagehelper.domain.Sort.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/2
 */
public class SortDeserializer extends JsonDeserializer<Sort> {
    public SortDeserializer() {
    }

    @Override
    public Sort deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ArrayNode arrayNode = (ArrayNode)jsonParser.getCodec().readTree(jsonParser);
        if (arrayNode != null && arrayNode.size() != 0) {
            List<Order> orders = new ArrayList(arrayNode.size());
            Iterator var5 = arrayNode.iterator();

            while(var5.hasNext()) {
                JsonNode jsonNode = (JsonNode)var5.next();
                String dirStr = jsonNode.get("direction").asText();
                String nhStr = jsonNode.get("nullHandling").asText();
                String property = jsonNode.get("property").asText();
                Direction direction = Direction.DESC.name().equalsIgnoreCase(dirStr) ? Direction.DESC : Sort.DEFAULT_DIRECTION;
                NullHandling nullHandling = NullHandling.valueOf(nhStr);
                Order order = new Order(direction, property, nullHandling);
                orders.add(order);
            }

            return new Sort(orders);
        } else {
            return null;
        }
    }
}
