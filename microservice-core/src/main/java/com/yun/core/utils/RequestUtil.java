package com.yun.core.utils;


/**
 * @Author: 听钱塘信起
 * @Data:2023/3/23 1:32
 * @version: 0.0.1
 */
public class RequestUtil {
    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    public RequestUtil() {
    }

    public static String getRequestDomain() {
        return null;
    }

    public static String getProtocol() {
        String baseUrl = getRequestDomain();
        if (baseUrl.startsWith("http://")) {
            return "http";
        } else if (baseUrl.startsWith("https://")) {
            return "https";
        } else {
            throw new IllegalArgumentException("base-url should start with 'http://' or 'https://'");
        }
    }
}
