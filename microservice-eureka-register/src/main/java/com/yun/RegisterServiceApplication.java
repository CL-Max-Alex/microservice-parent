package com.yun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


/**
 * @Author: 听钱塘信起
 * @Data:2023/3/18 15:47
 * @version: 0.0.1
 *  EurekaServer服务器端启动类，接受其它微服务注册进来
 */
@SpringBootApplication
@EnableEurekaServer
public class RegisterServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(RegisterServiceApplication.class,args);
    }
}
