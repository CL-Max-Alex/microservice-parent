package com.yun.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: 听钱塘信起
 * @Data:2023/3/18 16:00
 * @version: 0.0.1
 *  EnableDiscoveryClient //服务发现
 * 本服务启动后会自动注册进eureka服务中
 * scanBasePackages 自定义模块包描注
 */
@SpringBootApplication(
        scanBasePackages = {
                "com.yun.gateway",
                "com.yun.admin",
                "com.yun.oauth",
                "com.yun.core"
        }
)

@EnableEurekaClient
@EnableDiscoveryClient
@EnableSwagger2
public class GatewayServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayServiceApplication.class,args);
    }
}
