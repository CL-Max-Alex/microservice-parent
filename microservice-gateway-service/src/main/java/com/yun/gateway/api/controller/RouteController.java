package com.yun.gateway.api.controller;

import com.yun.gateway.infra.constant.Views;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/3/31
 * 路由跳转控制器
 */
@Controller
public class RouteController {

    @ApiOperation("跳转首页")
    @GetMapping({"/","/index"})
    public String toIndex(){
        return Views.INDEX;
    }
    @ApiOperation("跳转登录页")
    @GetMapping({"/toLogin"})
    public String toLogin(){
        return Views.LOGIN;
    }

    @ApiOperation("跳转注册页")
    @GetMapping({"/toRegister"})
    public String toRegister(){
        return Views.REGISTER;
    }



}
