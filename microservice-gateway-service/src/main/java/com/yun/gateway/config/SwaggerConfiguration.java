package com.yun.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: 听钱塘信起
 * @data:2023/3/23 1:22
 * @EnableSwagger2 开启swagger2
 * @version: 0.0.1
 */

@Configuration
@EnableSwagger2
@EnableOpenApi
public class SwaggerConfiguration {

    /**
     * 配置Docket实例
     * @return
     */
    @Bean
    public Docket getSwaggerDocket(){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yun.admin.api.controller"))
                .build()
                .pathMapping("/admin/")
                .groupName("admin-service");
    }

    /**
     * 配置Docket实例
     * @return
     */
    @Bean
    public Docket getOauthDocket(){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yun.oauth.api.controller"))
                .build()
                .pathMapping("/oauth/")
                .groupName("oauth-service");
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .contact(new Contact("听钱塘信来","https://blog.csdn.net/chenyunjiangNN","123@qq.com"))
                .title("Swagger在线文档")
                .version("1.0-SNAPSHOT")
                .license("localhost:8003")
                .licenseUrl("localhost")
                .build();
    }


}
