package com.yun.gateway.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


/**
 * 服务(Service)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:40:19
 */

@Getter
@Setter
@ApiModel("服务")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName( "hadm_service")
public class Service implements Serializable {
    private static final long serialVersionUID = 523144896205129858L;

    public static final String FIELD_SERVICE_ID = "serviceId";
    public static final String FIELD_SERVICE_CODE = "serviceCode";
    public static final String FIELD_SERVICE_NAME = "serviceName";
    public static final String FIELD_SERVICE_LOGO = "serviceLogo";

    @TableId
    private Long serviceId;

    @ApiModelProperty(value = "服务编码", required = true)

    private String serviceCode;

    @ApiModelProperty(value = "服务名称", required = true)

    private String serviceName;

    @ApiModelProperty(value = "服务图标")
    private String serviceLogo;


}

