package com.yun.gateway.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import lombok.Data;

import java.io.Serializable;


/**
 * 服务路由配置(ServiceRoute)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:40:59
 */

@Data
@ApiModel("服务路由配置")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("hadm_service_route")
public class ServiceRoute implements Serializable {
    private static final long serialVersionUID = 142071776330729620L;

    public static final String FIELD_SERVICE_ROUTE_ID = "serviceRouteId";
    public static final String FIELD_SERVICE_ID = "serviceId";
    public static final String FIELD_SERVICE_CODE = "serviceCode";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_PATH = "path";
    public static final String FIELD_URL = "url";
    public static final String FIELD_STRIP_PREFIX = "stripPrefix";
    public static final String FIELD_CUSTOM_SENSITIVE_HEADERS = "customSensitiveHeaders";
    public static final String FIELD_SENSITIVE_HEADERS = "sensitiveHeaders";
    public static final String FIELD_EXTEND_CONFIG_MAP = "extendConfigMap";

    @TableId
    private Long serviceRouteId;

    @ApiModelProperty(value = "服务ID", required = true)
    private Long serviceId;

    @ApiModelProperty(value = "路由对应的服务", required = true)
    private String serviceCode;

    @ApiModelProperty(value = "服务id，Route的标识，对应Route的id字段", required = true)
    private String name;

    @ApiModelProperty(value = "服务路径", required = true)
    private String path;

    @ApiModelProperty(value = "物理路径")
    private String url;

    private Integer stripPrefix;

    @ApiModelProperty(value = "是否自定义敏感头")
    private Integer customSensitiveHeaders;

    @ApiModelProperty(value = "敏感头部列表")
    private String sensitiveHeaders;

    @ApiModelProperty(value = "路由额外配置")
    private String extendConfigMap;


}

