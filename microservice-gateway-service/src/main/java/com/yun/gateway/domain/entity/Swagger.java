package com.yun.gateway.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * (Swagger)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:42:25
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("hadm_swagger")
public class Swagger implements Serializable {
    private static final long serialVersionUID = 612394083821849903L;

    public static final String FIELD_ID = "id";
    public static final String FIELD_SERVICE_NAME = "serviceName";
    public static final String FIELD_SERVICE_VERSION = "serviceVersion";
    public static final String FIELD_VALUE = "value";

    @TableId
    private Long id;

    @ApiModelProperty(value = "服务名,如hap-user-service", required = true)
    private String serviceName;

    @ApiModelProperty(value = "服务版本", required = true)
    private String serviceVersion;

    @ApiModelProperty(value = "接口文档json数据", required = true)
    private String value;


}

