package com.yun.gateway.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;


/**
 * @author HP
 */
@EnableWebSecurity
@EnableWebFluxSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class GatewaySecurityConfig {

        /**
         * 配置方式要换成 WebFlux的方式
         */
        @Bean
        public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity) {
            httpSecurity
                    .authorizeExchange().pathMatchers(HttpMethod.OPTIONS).permitAll()
                    // 任何请求需要身份认证
                    .pathMatchers("/**").permitAll().and()
                    .csrf().disable();
            return httpSecurity.build();
        }
}
