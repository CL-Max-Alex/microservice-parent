package com.yun.gateway.infra.constant;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/1
 *  页面路劲
 */
public interface Views {

    /**
     * 首页
     */
    public String INDEX ="index";

    /**
     * 登录页
     */
    public String LOGIN ="login";

    /**
     * 注册页
     */
    public String REGISTER ="register";


}
