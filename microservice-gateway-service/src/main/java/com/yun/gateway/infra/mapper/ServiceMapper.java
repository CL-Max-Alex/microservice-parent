package com.yun.gateway.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.gateway.domain.entity.Service;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 服务(Service)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:40:19
 */
@Mapper
public interface ServiceMapper extends BaseMapper<Service> {
    /**
     * 基础查询
     *
     * @param service 查询条件
     * @return 返回值
     */
    List<Service> selectList(Service service);
}

