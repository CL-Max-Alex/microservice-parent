package com.yun.gateway.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.gateway.domain.entity.ServiceRoute;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 服务路由配置(ServiceRoute)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:40:59
 */
@Mapper
public interface ServiceRouteMapper extends BaseMapper<ServiceRoute> {
    /**
     * 基础查询
     *
     * @param serviceRoute 查询条件
     * @return 返回值
     */
    List<ServiceRoute> selectList(ServiceRoute serviceRoute);
}

