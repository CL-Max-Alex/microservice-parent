package com.yun.gateway.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.gateway.domain.entity.Swagger;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Swagger)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-22 22:42:25
 */
@Mapper
public interface SwaggerMapper extends BaseMapper<Swagger> {
    /**
     * 基础查询
     *
     * @param swagger 查询条件
     * @return 返回值
     */
    List<Swagger> selectList(Swagger swagger);
}

