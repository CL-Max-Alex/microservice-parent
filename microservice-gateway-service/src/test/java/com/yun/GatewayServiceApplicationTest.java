package com.yun;

import com.yun.core.utils.StringToListUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


/**
 * @Author: 听钱塘信起
 * @Data:2023/3/22 17:03
 * @version: 0.0.1
 */
@Slf4j
@SpringBootTest
public class GatewayServiceApplicationTest {
  @Test
    public void testStringUtils(){
      String str = "FCreateOrgId,FUseOrgId,FNumber,FName,FDescription,FHelperCode,FLevel,FParentID,FDC,FGROUPID,FCreateDate,FModifyDate,FForbidStatus,FForbidDate,FITEMDETAILID,FAcctItemIsValid,FDataFieldName,FInputType,FAllCurrency,FCurrencyName,FCurrencyID";
      List<String> strings = StringToListUtils.stringToList(str);
      StringBuilder stringBuilder = new StringBuilder();
      strings.forEach(
              s -> {
                  stringBuilder.append("'");
                  stringBuilder.append(s);
                  stringBuilder.append("'");
                  stringBuilder.append(",");
              }
      );
      log.debug("strings====={}",strings);
      System.err.println(strings.size());
      System.err.println(stringBuilder);
  }
}
