package com.yun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/25
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
@EnableEurekaClient
public class MessageSharingServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessageSharingServiceApplication.class,args);
    }
}
