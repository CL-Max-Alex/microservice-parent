package com.yun.oauth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


/**
 * 启动类
 *
 * @author yunjiang.chen
 * @since 2023-03-24 00:44:23
 * 识别自定义注解
 */
@SpringBootApplication(scanBasePackages = {
        "com.yun.core",
        "com.yun.oauth"
})
@EnableEurekaClient
@MapperScan("com.yun.oauth.infra.mapper")
public class OauthServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OauthServiceApplication.class,args);
    }
}
