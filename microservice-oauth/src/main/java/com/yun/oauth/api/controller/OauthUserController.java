package com.yun.oauth.api.controller;


import com.yun.core.annotation.LogRequired;
import com.yun.core.utils.Results;
import com.yun.oauth.api.dto.UserDetailsDTO;
import com.yun.oauth.app.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
@Api(
        tags = "OauthUserController"
)
@RestController
@RequestMapping("/user")
public class OauthUserController {

    @Autowired
    private UserService userService;

    @ApiModelProperty("用户注册")
    @LogRequired
    @GetMapping({"/register"})
    public ResponseEntity<?> register(UserDetailsDTO userDetailsDTO){
       return Results.success(userService.userRegister(userDetailsDTO));
    }

    @ApiModelProperty("用户登录")
    @GetMapping({"/login"})
    public ResponseEntity<?> login(UserDetailsDTO userDetailsDTO){
        return Results.success(userService.userLogin(userDetailsDTO));
    }

}
