package com.yun.oauth.api.dto;

import com.yun.oauth.domain.entity.IUser;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/29
 */
@Data
@NoArgsConstructor
public class CustomerUserDetails implements UserDetails {



    IUser user;
    List<String> permissions;



    public CustomerUserDetails(IUser user, List<String> permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    /**
     * 权限集合:包括了角色信息以及可访问权限信息
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }



    @Override
    public String getPassword() {
        return user.getHashPassword();
    }

    @Override
    public String getUsername() {
        return user.getLoginName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
