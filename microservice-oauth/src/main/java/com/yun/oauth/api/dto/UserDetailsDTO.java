package com.yun.oauth.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
@Data
public class UserDetailsDTO {

    @ApiModelProperty(value = "用户名")
    private String loginName;

    @ApiModelProperty(value = "邮箱")
    private String email;


    @ApiModelProperty(value = "用户真实姓名")
    private String realName;

    @ApiModelProperty(value = "用户是否启用。1启用，0未启用")
    private Integer isEnabled;

    @ApiModelProperty(value = "密码")
    private String password;
}
