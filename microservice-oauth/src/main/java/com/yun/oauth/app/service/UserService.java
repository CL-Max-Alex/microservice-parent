package com.yun.oauth.app.service;

import com.yun.oauth.api.dto.UserDetailsDTO;
import com.yun.oauth.domain.entity.IUser;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
public interface UserService {


    /**
     * 用户登录
     * @param userDetailsDTO
     * @return
     */
    ResponseEntity<?> userLogin(UserDetailsDTO userDetailsDTO);

    /**
     * 用户注册
     * @param userDetailsDTO
     * @return
     */
    ResponseEntity<?> userRegister(UserDetailsDTO userDetailsDTO);
}
