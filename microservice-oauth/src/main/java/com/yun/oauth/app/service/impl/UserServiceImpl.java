package com.yun.oauth.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yun.oauth.api.dto.UserDetailsDTO;
import com.yun.oauth.app.service.UserService;
import com.yun.oauth.domain.entity.IUser;
import com.yun.oauth.domain.entity.IUserRole;
import com.yun.oauth.infra.config.CustomerSecurityConfig;
import com.yun.oauth.infra.mapper.IOauthUserRoleMapper;
import com.yun.oauth.infra.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private CustomerSecurityConfig customerSecurityConfig;

  @Autowired
  private IOauthUserRoleMapper userRoleMapper;

  @Override
  public ResponseEntity<?> userLogin(UserDetailsDTO userDetailsDTO) {
      try {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetailsDTO.getLoginName(), userDetailsDTO.getPassword());
        Authentication authenticate = authenticationManager.authenticate(token);
        //   存入SecurityContextHolder
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        return new ResponseEntity<>(HttpStatus.OK);
      }catch (Exception e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.UNAUTHORIZED);
      }

  }

  @Override
  public ResponseEntity<?> userRegister(UserDetailsDTO userDetailsDTO) {
    IUser user = new IUser();
    user.setRealName(userDetailsDTO.getRealName());
    user.setLoginName(userDetailsDTO.getLoginName());
    user.setEmail(userDetailsDTO.getEmail());
    user.setHashPassword(customerSecurityConfig.passwordEncoder().encode(userDetailsDTO.getPassword()));
    userMapper.insert(user);
    // 对注册用户初始化角色为学生 roleId=1
    LambdaQueryWrapper<IUser> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper
            .eq(IUser::getRealName,userDetailsDTO.getRealName())
            .eq(IUser::getLoginName,userDetailsDTO.getLoginName())
            .eq(IUser::getEmail,userDetailsDTO.getEmail());
    IUser userInDb = userMapper.selectOne(queryWrapper);
    IUserRole userRole = new IUserRole();
    userRole.setRoleId(Long.valueOf("1"));
    userRole.setUserId(userInDb.getId());
    userRoleMapper.insert(userRole);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
