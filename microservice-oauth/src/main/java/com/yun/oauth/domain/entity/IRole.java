package com.yun.oauth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色表(Role)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-28 22:24:36
 */

@Data
@ApiModel("角色表")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("hyun_role")
public class IRole  implements Serializable {
    private static final long serialVersionUID = -36681746954082067L;

    public static final String FIELD_ID = "id";
    public static final String FIELD_ROLE_NAME = "roleName";
    public static final String FIELD_ROLE_CODE = "roleCode";
    public static final String FIELD_IS_ENABLED = "isEnabled";


    @TableId
    private Long id;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色编码")
    private String roleCode;

    @ApiModelProperty(value = "用户是否启用。1启用，0未启用")
    private Integer isEnabled;



}

