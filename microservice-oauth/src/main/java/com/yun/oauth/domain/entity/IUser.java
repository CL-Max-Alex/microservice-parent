package com.yun.oauth.domain.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import lombok.Data;


/**
 * (User)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-24 00:44:23
 */

@ApiModel("用户表")
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName( "hyun_user")
public class IUser  implements Serializable {
    private static final long serialVersionUID = 383308255860227603L;

    public static final String FIELD_ID = "id";
    public static final String FIELD_LOGIN_NAME = "loginName";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_HASH_PASSWORD = "hashPassword";
    public static final String FIELD_REAL_NAME = "realName";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_IMAGE_URL = "imageUrl";
    public static final String FIELD_PROFILE_PHOTO = "profilePhoto";
    public static final String FIELD_LAST_PASSWORD_UPDATED_AT = "lastPasswordUpdatedAt";
    public static final String FIELD_LAST_LOGIN_AT = "lastLoginAt";
    public static final String FIELD_IS_ENABLED = "isEnabled";
    public static final String FIELD_IS_LOCKED = "isLocked";
    public static final String FIELD_IS_LDAP = "isLdap";
    public static final String FIELD_IS_ADMIN = "isAdmin";
    public static final String FIELD_LOCKED_UNTIL_AT = "lockedUntilAt";
    public static final String FIELD_PASSWORD_ATTEMPT = "passwordAttempt";


    @TableId
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String loginName;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "Hash后的用户密码")
    private String hashPassword;

    @ApiModelProperty(value = "用户真实姓名")
    private String realName;

    private String phone;

    @ApiModelProperty(value = "用户头像地址")
    private String imageUrl;

    @ApiModelProperty(value = "用户二进制头像")
    private String profilePhoto;

    @ApiModelProperty(value = "上一次密码更新时间", required = true)
    private Date lastPasswordUpdatedAt;

    @ApiModelProperty(value = "上一次登陆时间")
    private Date lastLoginAt;

    @ApiModelProperty(value = "用户是否启用。1启用，0未启用", required = true)
    private Integer isEnabled;

    @ApiModelProperty(value = "是否锁定账户", required = true)
    private Integer isLocked;

    @ApiModelProperty(value = "是否是LDAP来源。1是，0不是")
    private Integer isLdap;

    @ApiModelProperty(value = "是否为管理员用户。1表示是，0表示不是")
    private Integer isAdmin;

    @ApiModelProperty(value = "锁定账户截止时间")
    private Date lockedUntilAt;

    @ApiModelProperty(value = "密码输错累积次数")
    private Integer passwordAttempt;

}

