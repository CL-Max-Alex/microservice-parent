package com.yun.oauth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户角色关联表(UserRole)实体类
 *
 * @author yunjiang.chen
 * @since 2023-03-29 00:35:15
 */

@Data
@ApiModel("用户角色关联表")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("hyun_user_role")
public class IUserRole  implements Serializable {
    private static final long serialVersionUID = 200615853985830013L;

    public static final String FIELD_U_ID = "userId";
    public static final String FIELD_R_ID = "roleId";

    @TableId
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty(value = "角色id")
    private Long roleId;

}

