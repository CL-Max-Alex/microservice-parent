package com.yun.oauth.infra.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


/**
 * @author HP
 */
@Configuration
@EnableGlobalMethodSecurity(securedEnabled=true,prePostEnabled=true)
public class CustomerSecurityConfig {


    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Autowired
    private AuthenticationConfiguration authenticationConfiguration;

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().antMatchers("/images/**", "/js/**", "/webjars/**","/v2/**","/v3/**","/swagger-resource/**","/layui/**","/lib/**");
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                //关闭csrf
                .csrf().disable()
                .authorizeRequests()
                // 允许匿名访问的接口
                .antMatchers("/user/login").anonymous()
                .antMatchers("/user/register").anonymous()
                .antMatchers("/toLogin").anonymous()
                // 访问toUserAdd接口需要admin角色
                .antMatchers("/toUserAdd").hasAnyRole("admin")
                // 访问toUserEdit接口需要edit权限
//                .antMatchers("/toUserEdit").hasAnyAuthority("edit")
//                .antMatchers("/toUserEdit").hasAuthority("edit")
                .antMatchers("/toUserList").hasAnyAuthority("select")
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated();

        http.formLogin()
                // 访问登录页面接口
                .loginPage("/toLogin")
                // 执行登录方法接口
                .loginProcessingUrl("user/login");



//        http.logout().logoutUrl("/logout");
        return http.build();
    }


}
