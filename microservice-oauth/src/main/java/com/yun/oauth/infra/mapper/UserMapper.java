package com.yun.oauth.infra.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yun.oauth.domain.entity.IRole;
import com.yun.oauth.domain.entity.IUser;

import java.util.List;


/**
 * (User)应用服务
 *
 * @author yunjiang.chen
 * @since 2023-03-24 00:44:23
 */
public interface UserMapper extends BaseMapper<IUser> {

    /**
     * 查询权限
     * @param user
     * @return
     */
    List<String> selectPermission(IUser user);

    /**
     * 查询角色
     * @param user
     * @return
     */
    List<IRole> selectRole(IUser user);

}

