package com.yun.oauth;


import com.yun.oauth.domain.entity.IUser;
import com.yun.oauth.infra.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author: 听钱塘信起
 * @version: 0.0.1
 * @since :2023/3/24
 */
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class OauthServiceApplicationTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testQueryUser(){
        List<IUser> userList = userMapper.selectList(null);
        System.err.println(userList.size());
    }

    @Test
    public void testSaveUser(){
        IUser user = new IUser();
        user.setRealName("张三");
        user.setLoginName("张三1");
        user.setHashPassword("ZS");
        userMapper.insert(user);

    }


}
